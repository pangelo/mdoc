# mdoc - modular document generator

`mdoc` is a simple script to generate documents from modular document fragments written in Markdown, and then export the resulting document into many different formats.

## Usage

To use `mdoc` write the different fragments that compose your document as Markdown documents and put them into a folder (named `mdoc.d` by default, but this can be changed).

the command syntax is

    mdoc FORMAT [MODULES | PRESET]

where FORMAT is one of:

 - txt
 - md
 - pdf
 - doc
 - odt
 - html

and MODULES is a comma separated list of the basename of the Markdown
files in the `mdoc.d` folder.

As a quick demo try:

    mkdir ./mdoc.d
    echo "# My CV" > cv.d/title.md
	echo "## Education" > cv.d/education.md
	echo "## Experience" > cv.d/experience.md
    ./mkcv pdf title,education,experience
    xdg-open ./cv.pdf

To create presets and customise the data and output locations, create a file called `mdoc.conf` and copy the variables in the "Default configurations" section at the top of the `mdoc` script.

## Some useful references

 * [Editing a CV in markdown with pandoc](http://blog.chmd.fr/editing-a-cv-in-markdown-with-pandoc.html)
 * [The Markdown Resume](https://mszep.github.io/pandoc_resume/)
 * [Pandoc User Guide](http://pandoc.org/MANUAL.html)

